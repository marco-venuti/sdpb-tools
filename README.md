# SDPB tools
Collection of utilities for [SDPB](https://github.com/davidsd/sdpb).

## Installation
Just clone this repository (requires `git` to be installed)
```
git clone https://gitlab.com/marco-venuti/sdpb-tools <target directory>
```
If you want you can add the repo to your `PATH` so that you don't need to enter the full path of the executable. Add the following line to your `.bashrc`/`.zshrc`
```
PATH="<target directory>:$PATH"
```

## Usage
### Makefile

### sdpb-parallel
`sdpb-parallel` is a wrapper around GNU `parallel` (which of course must be installed). It connects to remote hosts via SSH and distributes SDPB runs at input file level. It takes care of setting up the right `parallel` arguments to copy the input files to the remote machine, and to copy the results back to your machine. SDPB itself runs remotely in parallel, one job at a time, using all available cores.

To use `sdpb-parallel`, we first need to setup the target machines. Create the file `~/.config/sdpb-nodes.conf` with something like this inside
```
1/hostname1.example.com
1/hostname2.example.com
```
The 1 in front is the number of concurrent jobs (i.e. input files) per host. Note that you can use hostname shorthands defined in your custom `~/.ssh/config`.
We can then open the SSH sessions to the target hosts, which will continue running in background (internally this uses `ControlMaster`)
```
sdpb-parallel connect
```
Suppose now we have a directory called `xml` where we have a collection of files whose names match the pattern
```
input_<number>.xml
```
and which contain SDPB input as produced by the [Mathematica interface](https://raw.githubusercontent.com/davidsd/sdpb/master/docs/SDPB-Manual.pdf).
We can start the computations by
```
sdpb-parallel run xml
```
Replace `xml` with the path to the directory containing your input files as needed.

During the run, a progress bar can be seen at the bottom of the terminal. At the end, two files will be produced in your current working directory:
- `result.txt`: each line corresponds to an input file and has two columns: the first is the `<number>` in the filename. The second is the `primalObjective` produced by SDPB (even if the exit reason is not `found primal-dual optimal solution`)
- `reason.txt`: same as above, but the second column is the `terminateReason` for each file (so that we know e.g. what points we should discard).

When we are done we can close all sockets by
```
sdpb-parallel disconnect
```
Note that if you have open connections and your network goes down, the sockets become stale and you need to disconnect and reconnect again.
