#!/usr/bin/env bash

XMLDIR=/usr/local/share/sdpb
SDPDIR=/usr/local/share/sdpb/sdp
N_PROC=$1
PRECISION=$2

MPICMD="mpirun -n $N_PROC"

mkdir -p $SDPDIR

for i in $XMLDIR/*.xml; do
    echo "Running pvm2sdp on $i"
    pvm2sdp $PRECISION $i $SDPDIR/$(basename -s .xml -- $i).sdp
done

for i in $SDPDIR/*; do
    echo "Running sdpb on $i"
    $MPICMD sdpb --precision=$PRECISION --procsPerNode=$N_PROC -s $i
done
